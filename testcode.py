#%%
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import colour as clr
import sys
import casadi as ca
from IPython.display import Markdown
sys.path.insert(0,'parameterFiles')
# %matplotlib widget 
#%config InlineBackend.figure_formats = ['svg']
import ReachingModels as reaching


sim = reaching.Kinarm()
op = sim.movementTimeOptSetup(
  theTimeValuation  = .1,
  theN              = 100, 
  theFRCoef         = 0.085,
  thediscreteOrCont = 'discrete')

result = sim.updateGuessAndSolve(
  op,
  xystart = np.array([0.1,0.2]),
  xyend = np.array([0.1,0.4]),
  theDurationGuess = 1.0,
  theTimeValuation = 10.0,
  theGeneratePlots = 1,
  theFRCoef = 0.085)
#%%
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import colour as clr
import sys
import casadi as ca
from IPython.display import Markdown
sys.path.insert(0,'parameterFiles')
# %matplotlib widget 
#%config InlineBackend.figure_formats = ['svg']
import ReachingModels as reaching

eye = reaching.eyeHW2()
optiPrev = eye.movementTimeOptSetup(
      theN              = 100, 
      theFRCoef         = 40)
c_t1 = 20
c_fr1 = 0.01
traj, optiOrig = eye.updateGuessAndSolve(
      optiPrev, 
      np.array([0]), 
      np.array([0]) + np.array(.01),
      theDurationGuess    = .1,
      theTimeValuation    = c_t1,
      theGeneratePlots    = 1,
      theFRCoef           = c_fr1)
#%%
f,ax = plt.subplots()
e       = eye.energy(traj.Q,traj.QDot,traj.U,traj.time)
p_damp  = traj.QDot * traj.QDot * eye.k2
ax.plot(traj.time, e.e_jointAll.T)
ax.plot(traj.time, e.e_k)
ax.plot(traj.time,e.damp)
ax.plot(traj.time,e.e_k + e.damp)
# legend
ax.legend(['e_jointAll','e_k','e_damp','total'])

#%%
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import colour as clr
import sys
import casadi as ca
from IPython.display import Markdown
sys.path.insert(0,'parameterFiles')
# %matplotlib widget 
#%config InlineBackend.figure_formats = ['svg']
import ReachingModels as reaching

sim = reaching.Kinarm()
sim.l1 = .35
sim.l2 = .45
sim.l = [sim.l1, sim.l2]

c_t = 10
x0  = 0.0
y0  = 0.2
x1  = x0
y1  = y0

waypoints = [np.array([x0,y0]),np.array([x0+.0,y0+.05]),np.array([x0+.0,y0+.1]),np.array([x1,y1])] #np.array([x0+.05,y0+.05])
# convert waypoints into waypoints_q using inverse kinematics
waypoints_q = [sim.xy2joints(waypoints[i] ) for i in range(len(waypoints))]

c_fr = 0.085

def initAndEndZeros(opti,list):
  for var in list:
    for dof in range(0,var.shape[0]):
      opti.subject_to(var[dof,0] == 0.0)
      opti.subject_to(var[dof,-1] == 0.0)

def initAndEndMatch(opti:ca.Opti, thelist):
    for var in thelist:
      for dof in range(0, var.shape[0]):
        opti.subject_to(var[dof,0] == var[dof,-1])

f,ax = plt.subplots()

traj_list = list()
twrap = 0
for i in range(0,len(waypoints)-1):
  
  oP = sim.movementTimeOptSetup(
    theTimeValuation  = .1,
    theN              = 100, 
    theFRCoef         = c_fr,
    thediscreteOrCont = 'discretephase')

  # manually handle opti constraints for the beginning and end of each submovement. 
  if i == 0: 
    # oP.ddqdt2, pddu, nddu are all zero at the beginning of the first submovement
    oP.opti.subject_to(oP.ddudt2[:,0] == 0)
    oP.opti.subject_to(oP.ddqdt2[:,0] == 0)
    oP.opti.subject_to(oP.pddu[:,0] == 0)
    oP.opti.subject_to(oP.nddu[:,0] == 0)
    oP.opti.subject_to(oP.ddudt2[:,-1] == 0)
    oP.opti.subject_to(oP.ddqdt2[:,-1] == 0)

    print('setting constraints for first submovement')
  
  if (i > 0) & (i < (len(waypoints)-2)):
    oP.opti.subject_to(oP.ddudt2[:,0] == oP.ddudt2[:,-1])
    oP.opti.subject_to(oP.ddqdt2[:,0] == oP.ddqdt2[:,-1])
    print('setting constraints for intermediate movement')
  
  if i == len(waypoints)-2:                      
    oP.opti.subject_to(oP.ddudt2[:,-1] == 0)
    oP.opti.subject_to(oP.ddqdt2[:,-1] == 0)
    oP.opti.subject_to(oP.ddqdt2[:,-2] == 0)  # 2022-11. here we are accounting for hermite-simpson using fwd-estimates. 
    oP.opti.subject_to(oP.dudt[:,-2] == 0)    # 2022-11. here we are accounting for hermite-simpson using fwd-estimates. 
    oP.opti.subject_to(oP.pddu[:,-1] == 0)
    oP.opti.subject_to(oP.nddu[:,-1] == 0)
    print('setting constraints for last submovement')

  xy0 = waypoints[i]
  xy1 = waypoints[i+1]

  trajopt, optiOrig = sim.updateGuessAndSolve(
    oP, 
    xy0, 
    xy1,
    theDurationGuess    = 1.0,
    theTimeValuation    = c_t,
    theGeneratePlots    = 0,
    theFRCoef           = c_fr)
  ax.plot(trajopt.time+twrap,trajopt.vecspeed)
  twrap = twrap + trajopt.time[-1]
  traj_list.append(trajopt)
f.show()



  # stack the results of the optimization
    
# %%
