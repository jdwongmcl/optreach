#%% Loop across distances and valuations to generate double-pendulum distance/time predictions. 
import ReachingModels as reaching
import numpy as np
import matplotlib.pyplot as plt
import colour as clr
import plotpresentlib as pp
from matplotlib.figure import Figure

DOSAVE = 0
savename = 'out50_fill'
loopdist = np.linspace(0.01,0.5,num=50)
loopValuation = np.concatenate((np.array([.05,0.08,0.1,.25,.5,.75]),np.linspace(1,50,num=50),np.array([100,150,200,250,300,350,400, 450, 500])))

loopdist = np.array([0.1])
loopValuation = np.array([10])

# %config InlineBackend.figure_formats = ['svg']
import sys
sys.path.insert(0,'parameterFiles')

# model that has equations to compute endpoint jacobians, equations of motion, and energy. 
sim = reaching.Kinarm()
sim.l1 = 0.35
sim.l = [sim.l1,sim.l2]
#sim = reaching.PointMass()

# starting x and y
x = 0.0
ys = 0.25
xystart = np.array([x,ys])



# create color gradient tvalcolors.
color1 = clr.Color("#e0f3db")
tvalcolors = list(color1.range_to(clr.Color("#084081"),len(loopValuation)))

### allocate for storage:durations, speeds, costs, rewards, solved
M = len(loopValuation)
N = len(loopdist)
duration      = np.zeros([M, N])
peakhandspeed = np.zeros([M, N])
J             = np.zeros([M, N])
costFR        = np.zeros([M, N])
costWork      = np.zeros([M, N])
costTime      = np.zeros([M, N])
solved        = np.zeros([M, N])
distance      = np.zeros([M, N])
timeValuation = np.zeros([M, N])

# initialize trajectories as a 1d list of 1d lists. 
traj = [[0 for x in range(N)] for y in range(M)] 

### SETUP
optiPrev = sim.movementTimeOptSetup(
  theTimeValuation  = 1.0,
  theN              = 60,   #2022-10: 20 and 40 both work flawlessly. this is after removing accel = 0 constraint. 
  theFRCoef         = 0.085)

### SOLVE
trajOrig, optiOrig = sim.updateGuessAndSolve(
  optiPrev, 
  xystart, 
  xystart + np.array([x,.10]),
  theDurationGuess    = 1,
  theTimeValuation    = 1,
  theGeneratePlots    = 0)

dGuess = trajOrig.duration

#