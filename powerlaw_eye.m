syms Ct Cf C0 L M T V

% energy = work_st + work_v3ploss
% eq1 = C0*M*D^2*T^(-3) + D^3*T^(-3) + Ct ==0 ;

% power loss in damping: F*V -> V*V. L^2T^-2. integrated over movement, L^2T^-1. d/dT: L^2T^-2.
eq1 = -C0*L^2/T^2 - Cf*L*T^(-4) + Ct ==0 ;
% eq1 = Cf*M*L*T^(-4)+ Ct ==0 ;


eqT = solve(eq1,T);
eqT_real = eqT(2);%should be (-(D^3 + C0*M*D^2)/Ct)^(1/3)

eq2 = subs(eq1,T,L/V);

eqV = solve(eq2,V);
eqV_real = eqV(1); % should be (-(Ct*D)/(D + C0*M))^(1/3)


%%
c0 = 1;
cf = 1;
ct = 1;
l = 0:0.01:1;
% v=(-(l.^3.*(c0 + ((c0.^2*l.^3 + 4*cf*ct)./l.^3).^(1/2)))/(2*cf)).^(1/2)
% v=-(-(l.^3.*(c0 + ((c0.^2*l.^3 + 4*cf*ct)./l.^3).^(1/2)))/(2*cf)).^(1/2)
v= (-(l.^3.*(c0 - ((c0.^2*l.^3 + 4*cf*ct)./l.^3).^(1/2)))/(2*cf)).^(1/2);
% v=-(-(l.^3.*(c0 - ((c0.^2*l.^3 + 4*cf*ct)./l.^3).^(1/2)))/(2*cf)).^(1/2)
figure();
plot(l,v)