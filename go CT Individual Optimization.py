#%% compute CT for each subject's Everyday and InfP data.
# main goal: to see if the CTs are the same for each subject, independent of the context.

import numpy as np
import matplotlib.pyplot as plt
import colour as clr
from matplotlib.figure import Figure
import os

import plotpresentlib as pp
import ReachingModels as reaching
import SimpleOpt as so
import PreferredReachExperiment as prefdur


#% load in the file '2023-9-7-AllContextsSummarymovtab.mat' and extract the data
# This file contains the data from the 2023-9-7 analysis.

import scipy.io as sio
reachingPath = os.path.dirname(reaching.__file__)

fmodel = 'modelSubj'+str(i)+'.mat'
fdata = 'mainSeqByTgtedaysubj'+ str(i) +'.mat'
sdat = sio.loadmat(reachingPath + '/empirical/subdata/'+fdata) #load test data (change path)
model = reaching.Kinarm(reachingPath + '/empirical/subdata/'+fmodel)

f,ax = plt.subplots()
ax = plt.subplot(1,1,1,projection = 'polar')
normDur = sdat["duration"]/sdat["perfdistance"]
normVel = sdat["peakspeed"]/sdat["perfdistance"]
ang = sdat["r_ang"]*180/np.pi
# radial plot of normVel and ang, where ang is in degrees
ax.plot(ang,normVel,'.')

# extract start and end
xystart   = sdat["xymeanstart"]
xyend     = sdat["xymeanend"]
duration  = sdat["duration"]
peakspeed = sdat["peakspeed"]
distance  = sdat["perfdistance"]

#%% verify that using a fixed c_t is the same as optimizing for c_t.
vec_c_t       = np.zeros([xystart.shape[0]])
duration_chk  = np.zeros([xystart.shape[0]])

for j in range(0,xystart.shape[0]):
  xystartj = xystart[j,:]
  xyendj = xyend[j,:]
    # optimize for a fixed C_T. 
  optVars = model.movementTimeOptSetup(theN = 20, theDuration = duration[j])
  solP, optRes = model.updateGuessAndSolve(optVars, xystartj, xyendj, theGeneratePlots = 0)  
  print('c_t = '+ str(solP.con_time))
  vec_c_t[j] = solP.con_time  
  # the
  optVars           = model.movementTimeOptSetup(theN = 20)
  solP, optRes      = model.updateGuessAndSolve(optVars, xystartj, xyendj, theTimeValuation = solP.con_time, theGeneratePlots = 0)
  duration_chk[j]   = solP.duration

  #%
  #f, ax= plt.subplots()
  #ax.plot(solP.time,solP.handspeed)

f,ax = plt.subplots()
plt.plot(duration_chk,duration,'.')

# def ctmean_condition(condition = 'eday', dataset = 'ByTgt'):
# constrain for movement time, fit ct for each reach in the set. 
# return the mean of the ct. also could optionaly return all ct.
def ctmean_condition(condition = 'eday', dataset = 'ByTgt'):
  msFit = []
  ctmean = []
  msData = []
  for i in range(1,10):
    reachingPath = os.path.dirname(reaching.__file__)
    fmodel = 'modelSubj'+str(i) + '.mat'
    model = reaching.Kinarm(reachingPath + '/empirical/subdata/'+fmodel)
    fdata = 'MainSeq' + condition + 'subj' + str(i) + dataset + '.mat'
    
    sdat = sio.loadmat(reachingPath + '/empirical/subdata/'+fdata,kind=dataset) #load test data (change path)
    ms_hum = prefdur.readMSFromMatStruct(sdat,kind=dataset)
    
    xystart   = ms_hum["xymeanstart"]
    xyend     = ms_hum["xymeanend"]
    duration  = ms_hum["duration"].flatten()
    peakspeed = ms_hum["peakspeed"].flatten()
    distance  = ms_hum["perfdistance"].flatten()
    msData.append(prefdur.MainSeq(distance = distance, duration = duration, peakspeed = peakspeed))
    ms, ct = prefdur.fit_mean_ct_oc(model, xystart, xyend, prefdur.MainSeq(distance = distance, duration = duration, peakspeed = peakspeed))
    msFit.append(ms)
    ctmean.append(ct)
  return msData,msFit,ctmean
#%%

dataset = 'ByTgt'
#dataset = 'AllMov'

#%% Eday fit. Mean data.
msData,msFit,ct=ctmean_condition(condition = 'Eday', dataset = dataset)
# list comprehension to compute rms of msFit.peakspeed and msData.peakspeed
rmsPeakspeed = [prefdur.perc_rms(msFit[i].peakspeed,msData[i].peakspeed) for i in range(0,9)]
# list comprehension to compute rms of msFit.duration and msData.duration
rmsDuration = [prefdur.perc_rms(msFit[i].duration,msData[i].duration) for i in range(0,9)]

#%% InfP fit. Mean data.
msDataInfP,msFitInfP,ctInfP =ctmean_condition(condition = 'InfP',dataset = dataset)
# list comprehension to compute rms of msFit.peakspeed and msData.peakspeed
rmsPeakspeedInfP = [prefdur.perc_rms(msFitInfP[i].peakspeed,msDataInfP[i].peakspeed) for i in range(0,9)]
# list comprehension to compute rms of msFit.duration and msData.duration
rmsDurationInfP = [prefdur.perc_rms(msFitInfP[i].duration,msDataInfP[i].duration) for i in range(0,9)]

#use scipy.io write msData, msFit, ct, msDataInfP, msFitInfP, ctInfP rmsPeakspeed, rmsDuration, rmsPeakspeedInfP, rmsDurationInfP to file fname, defined below, prefixed by today's date
import datetime
today = datetime.date.today()
todaystr = today.strftime("%Y-%m-%d")
fname = todaystr+'ctmean_condition.mat'
fname = reachingPath + '/empirical/pythonout/'+fdata
sio.savemat(fname,{'msData':msData,'msFit':msFit,'ct':ct,'msDataInfP':msDataInfP,'msFitInfP':msFitInfP,'ctInfP':ctInfP,'rmsPeakspeed':rmsPeakspeed,'rmsDuration':rmsDuration,'rmsPeakspeedInfP':rmsPeakspeedInfP,'rmsDurationInfP':rmsDurationInfP})



