#%% Functions meant to handle simulating the reach experiment. 
import ReachingModels as reaching
import SimpleOpt as so
import numpy as np
import matplotlib.pyplot as plt
import colour as clr
import plotpresentlib as pp
from matplotlib.figure import Figure
import scipy.io as scio
import math as math
# container for the main sequence data.
class MainSeq:
  distance  = np.array([])
  peakspeed = np.array([])
  distance  = np.array([])
  xystart   = np.array([])
  xyend     = np.array([])
  trajlist  = list()
  def __init__(self, distance = [], peakspeed = [], duration = [], xystart = [], xyend = [], trajlist = []):
    self.distance   = distance
    self.peakspeed  = peakspeed
    self.duration   = duration
    self.xystart    = xystart
    self.xyend      = xyend
    self.trajlist   = trajlist

def rms(x,y):
  if x.shape != y.shape:
    print("shapes unequal. Flattening.")
  x = x.flatten()
  y = y.flatten()
  return np.sqrt(np.nanmean(np.square(x-y)))

def perc_rms(x,y):
  if x.shape != y.shape:
    print("shapes unequal. Flattening.")
  x = x.flatten()
  y = y.flatten()
  return np.sqrt(np.nanmean(np.square(x-y)))/np.nanmean(x)

def R2(x,y):
  if x.shape != y.shape:
    print("shapes unequal. Flattening.")
  x = x.flatten()
  y = y.flatten()
  return 1 - np.nansum(np.square(x-y))/np.sum(np.square(x-np.nanmean(x)))

# fit a mean ct to the provided xy0, xy1 and ms data. 
# return the median ct, and the fitted ms.
def fit_mean_ct_oc(model:so.SimpleModel,the_xy0,the_xy1,ms:MainSeq):
  vec_c_t       = np.zeros([the_xy0.shape[0]])
  curspeed      = np.zeros([the_xy0.shape[0]])
  curduration   = np.zeros([the_xy0.shape[0]])

  for i in range(0,the_xy0.shape[0]):
    xystarti = the_xy0[i,:]
    xyendi   = the_xy1[i,:]
    # optimize for a fixed C_T. 
    optVars = model.movementTimeOptSetup(theN = 20, theDuration = ms.duration[i])
    solP, optRes = model.updateGuessAndSolve(optVars, xystarti, xyendi, theGeneratePlots = 0)  
    vec_c_t[i] = solP.con_time
    curspeed[i] = solP.peakspeed
    curduration[i] = solP.duration

  cur_ct = np.median(vec_c_t)
  speed_fix      = np.zeros([the_xy0.shape[0]])
  duration_fix   = np.zeros([the_xy0.shape[0]])
  for i in range(the_xy0.shape[0]):
    xystarti = the_xy0[i,:]
    xyendi   = the_xy1[i,:]
    # optimize for a fixed C_T. 
    optVars = model.movementTimeOptSetup(theN = 20, theTimeValuation=cur_ct)
    solF, optRes = model.updateGuessAndSolve(optVars, xystarti, xyendi, theTimeValuation=cur_ct, theDurationGuess = curduration[i],theGeneratePlots = 0)  

    speed_fix[i] = solF.peakspeed
    duration_fix[i] = solF.duration

  f,ax = plt.subplots(3,2,figsize = (5,5))
  f.tight_layout(h_pad=2,w_pad = 2)
  ax[0,0].plot(ms.distance,ms.duration,'.')
  ax[0,0].plot(ms.distance,duration_fix,'.')
  ax[0,1].plot(ms.distance,ms.peakspeed,'.')
  ax[0,1].plot(ms.distance,speed_fix,'.')
  return MainSeq(distance = ms.distance, duration = duration_fix, peakspeed = speed_fix), cur_ct

def simulatePreferredReaches(dists = [0.05,.1,0.15,0.2,0.25,0.3],xystart=np.array([-.05,0.2]), l2 = .45,
  timeValuation = 15, n = 60, fr = 8.5e-2, savename = 'resultsTrajectories.mat'):
  # %config InlineBackend.figure_formats = ['svg']
  # model that has equations to compute endpoint jacobians, equations of motion, and energy. 
  sim = reaching.Kinarm()
  sim.l2 = l2
  sim.l1 = 0.35
  sim.l = np.array([sim.l1,sim.l2])
  optiPrev = sim.movementTimeOptSetup(
  theTimeValuation  = timeValuation,
  theN              = n,   
  theFRCoef         = fr)
  
  trajList = list()  
  duration = np.zeros([len(dists)])
  peakspeed = np.zeros([len(dists)])
  distance = np.zeros([len(dists)])

  for count,d in enumerate(dists):
    print("i" + str(d))
    xyend = xystart + np.array([0,d])
    temptraj, optiPrev = sim.updateGuessAndSolve(optiPrev,xystart = xystart, xyend = xyend, 
      theTimeValuation=timeValuation,theGeneratePlots=0)
    trajList.append(temptraj)
    duration[count]  = temptraj.time[-1]
    peakspeed[count] = max(temptraj.vecspeed)
    distance[count] = d

  saveDict = {
    "traj":trajList,
    "peakspeed":peakspeed,
    "distance":distance,
    "duration":duration,
    "timeValuation":timeValuation}

  scipy.io.savemat('simulationResults/'+savename, saveDict)
  return trajList, sim, saveDict

def readMSFromMatStruct(in_dict:dict,kind:str):
  if kind == 'ByTgt':
    xystart   = in_dict["xymeanstart"]
    xyend     = in_dict["xymeanend"]
    duration  = in_dict["duration"].flatten()
    peakspeed = in_dict["peakspeed"].flatten()
    distance  = in_dict["perfdistance"].flatten()
    return MainSeq(distance = distance, duration = duration, peakspeed = peakspeed, xystart = xystart, xyend = xyend)

  elif kind == 'AllMov':
    xystart   = in_dict["startxy"]
    xyend     = in_dict["endxy"]
    distance  = in_dict["distance"].flatten()
    duration  = in_dict["duration"].flatten()
    peakspeed = in_dict["peakspeed"].flatten()
    return MainSeq(distance = distance, duration = duration, peakspeed = peakspeed, xystart = xystart, xyend = xyend)

def optimalReachesForCT(data:MainSeq, model:so.SimpleModel,ct, theN=20):
  # def magictrick(ms:MainSeq,model:so.SimpleModel,ct):
  # following random selection of the movement, 
  
  # foreach movement
  # use the individual movement start and end, 
  # simulate to get optimal, 
  # save the data. 
  
  # then, for each result
  # correlate with the actual data.  
  optVars = model.movementTimeOptSetup(theN = theN, theTimeValuation=ct)
  
  duration  = np.zeros(data.distance.shape[0])
  peakspeed = np.zeros(data.distance.shape[0])
  distance  = np.zeros(data.distance.shape[0])
  trajlist  = []
  for i in range(0,data.distance.shape[0]):
    xystarti = data.xystart[i]
    xyendi   = data.xyend[i]
    if not(math.isnan(data.duration[i])):
      solP, optRes = model.updateGuessAndSolve(optVars, 
                                             xystarti, 
                                             xyendi, 
                                             theTimeValuation=ct, 
                                             theDurationGuess = data.duration[i],theGeneratePlots = 0)  
      # save solP.duration, .distance, .peakspeed, and .handspeed trajectory
      duration[i] = solP.duration
      distance[i] = solP.distance
      peakspeed[i] = solP.peakspeed
      trajlist.append(solP)
  
  return MainSeq(distance = distance, duration = duration, peakspeed = peakspeed, trajlist = trajlist)



#####################
# tvals = np.array([0.05,0.08,0.1,0.25,0.5,0.75])
# ls = np.linspace(0.01,0.35,35)

# duration  = np.zeros([tvals.shape[0],ls.shape[0]])
# peakspeed = np.zeros([tvals.shape[0],ls.shape[0]])
# distance  = np.zeros([tvals.shape[0],ls.shape[0]])
# timeValuation = np.zeros([tvals.shape[0],ls.shape[0]])
  
# DOSAVE = 1
# for itval,val in enumerate(tvals):
  
#   sname = "trajWtval0p"+str(int(val*100))+".mat"
#   trajs, sim, saveDict = simulatePreferredReaches(dists=ls,
#     timeValuation = val, savename=sname)
  
#   # some printing and storing of data
#   print("-------------------- completed" + str(int(itval*100)) + " --------------------")
#   print("--------------------------------------------------------------------------------")
#   print("--------------------------------------------------------------------------------")  

# %%
