---
title: "Directional sensitivity of the kinarm model"
format:
  ipynb:
    default-image-extension: svg
  html:
    default-image-extension: svg
    code-fold: true
  pdf:
    default-image-extension: pdf
    documentclass: scrartcl
    pdf-engine-opt: --shell-escape
    mainfont: Minion Pro   # disable this if you don't have the font
    mathfont: Minion Pro   # 
jupyter: python3
execute:
  keep-ipynb: true
# filters:
#     - abstract-section
bibliography: references.bib
csl: nature-neuroscience-brief-communications.csl
---

# Positional sensitivity.
In @shadmehr_representation_2016, they report a directional sensitivity to movement speed and duration that is substantial.  Here we show that this sensitivity is present in the kinarm model, but that it heavily depends on the position of the arm. 

```{python}
#| label: fig-sweep1
#| fig-cap: "Sweep across c_t, c_fr (using displacement via optimalreaching.jl)."
#| 
#%% Loop across distances and valuations to generate double-pendulum distance/time predictions. 
import ReachingModels as reaching
import numpy as np
import matplotlib.pyplot as plt
import colour as clr
import plotpresentlib as pp
from matplotlib.figure import Figure

# define the reaching model.
sim = reaching.Kinarm()
sim.l1 = 0.35
sim.l2 = 0.35
sim.l = [sim.l1,sim.l2]

# loop across start and end positions, simulate and return duration, peakhandspeed, J, costFR, costWork, costTime, solved, distance, timeValuation
def loopXYStartEndSims(xy0,xy1,c_fr = 0.085,the_Nodes = 20,c_t = 1.0):
  M = xy0.shape[0]
  duration      = np.zeros([M])
  peakhandspeed = np.zeros([M])
  J             = np.zeros([M])
  costFR        = np.zeros([M])
  costWork      = np.zeros([M])
  costTime      = np.zeros([M])

  solved        = np.zeros([M])
  distance     = np.zeros([M])
  timeValuation = np.zeros([M])

  ### setup the optimization. 
  optiPrev = sim.movementTimeOptSetup(
    theTimeValuation  = c_t,
    theN              = the_Nodes, #more accuracy with more points.    
    theFRCoef         = c_fr)
  i = 0
  for (curxy0,curxy1) in zip(xy0,xy1):
    c_t_fixed = 15
    ### solve the opt once, we can then always use optiOrig as initial guess. 
    solP, optiOrig = sim.updateGuessAndSolve(optiPrev, 
    curxy0,
    curxy1,
    theDurationGuess    = 1,
    theTimeValuation    = c_t_fixed,
    theGeneratePlots    = 0)
    # unpack the solution into the variables we want as defined at the beginning of loopXYStartEndSims
    duration[i]      = solP.duration
    peakhandspeed[i] = solP.peakspeed
    J[i]             = solP.costJ
    costFR[i]        = solP.costFR
    costWork[i]      = solP.costWork
    costTime[i]      = solP.costTime
    solved[i]        = solP.solved
    distance[i]      = solP.distance
    timeValuation[i] = c_t_fixed
    i += 1
  return distance, duration, peakhandspeed, timeValuation, J, costFR, costWork, costTime, solved, 

def plotAngularDependence(ang,distance, dur, peakspeed,J,costfr,costw,costt,ct,solved):
  f,ax = plt.subplots(1,2,figsize = (5,5))
  

  # set the first subplot to be a polar plot, and plot the duration as a function of ang8
  ax[0] = plt.subplot(1,2,1,projection = "polar")
  ax[0].plot(ang,dur,linestyle = None,linewidth = 0, marker = ".")
  ax[0].set_rmax(max(dur)*1.1)
  ax[0].set_title("Duration")

  # plot the peakspeed as a function of ang8
  ax[1] = plt.subplot(1,2,2,projection = "polar")
  ax[1].plot(ang,peakspeed,linestyle = None,linewidth = 0, marker = ".")
  ax[1].set_rmax(max(peakspeed)*1.1)
  ax[1].set_title("Peak speed")
  
  f.tight_layout(h_pad=3, w_pad = 3)
  plt.show()
#call plotAngularDependence
def plot_positional_dependence_shadmehr2016(xyfixed,r=.1,steps = 16):
  angs    = np.arange(0,2*np.pi,np.pi/steps)
  xyend   = np.array([xyfixed[0]+r*np.cos(angs),xyfixed[1]+r*np.sin(angs)]).T
  xystart = np.ones(xyend.shape)*xyfixed
  distance, dur, peakspeed,J,costfr,costw,costt,ct,solved = loopXYStartEndSims(xystart,xyend)
  plotAngularDependence(angs,distance, dur, peakspeed,J,costfr,costw,costt,ct,solved)


# %%
step = 16
plot_positional_dependence_shadmehr2016([-.2,0.35],r=.1,steps = step)

plot_positional_dependence_shadmehr2016([0,0.35],r=.1,  steps = step)

plot_positional_dependence_shadmehr2016([0.20,.35],r=.1,steps = step)
# %%
```

The three largest c_fr (.1,1,10) make bell-shaped curves; below it starts to get pretty triangular. <br>
### Second sweep: fixed c_fr, adjusted distances and c_t 
Keeping c_fr = `{py cfr_best}` fixed, now we look at effect of c_t (but keeping near c_t = 10).
Also, distances are kept the same.


# References