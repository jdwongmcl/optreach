#%% 2023-10-03:


import ReachingModels as reaching
import numpy as np
import matplotlib.pyplot as plt
import colour as clr
import plotpresentlib as pp
from matplotlib.figure import Figure
import scipy.io as io 
import sys
from pathlib import Path
import os
gitdir = '/Users/jeremy/Git/optreach'
foldername = 'parameterFiles'
sys.path.insert(0, Path(gitdir,foldername))
fname = "billystartendxy.mat"
data = io.loadmat(Path(gitdir,foldername,fname))
#%%
tvslow = np.array([0.05,0.075,0.1,0.5])
tvelse  = np.linspace(1.0,250.0,250)
timeValuations  = np.hstack((tvslow,tvelse))

m = timeValuations.shape[0]

# %% movements 
sim = reaching.Kinarm()
sim.l = [0.35,0.45]
sim.l1 = sim.l[0]
sim.l2 = sim.l[1]

xyse = data["startend"]

n = xyse.shape[0]
#%%
trajList = list()  
duration = np.zeros([m*n])
peakspeed = np.zeros([m*n])
distance = np.zeros([m*n])
timeValuation = np.zeros([m*n])
moveNumber = np.zeros([m*n])

frcoefnom = 8.5e-2
frcoefgain = 1
frcoef = frcoefnom*frcoefgain
optiP       = sim.movementTimeOptSetup(theN=20)

for (iIndT,curTVal) in enumerate(timeValuations):
  for count in range(0,n):
    xystart = xyse[count,0:2]
    xyend   = xyse[count,2:4]
    r_mov   = xyend-xystart
    temptraj,tempopti = sim.updateGuessAndSolve(oP=optiP,
      xystart = xystart, 
      xyend=xyend,
      theTimeValuation = curTVal,
      theFRCoef = frcoef,
      theGeneratePlots = 0)
    trajList.append(temptraj)
    duration[count + iIndT*n]  = temptraj.time[-1]
    peakspeed[count + iIndT*n] = max(temptraj.vecspeed)
    distance[count + iIndT*n]  = np.sqrt(r_mov[0]**2+r_mov[1]**2)
    timeValuation[count + iIndT*n]  = curTVal
    moveNumber[count + iIndT*n]  = count
# %%
fig,ax =plt.subplots(2,2)
for tt in trajList:
  if(tt.solved):
    ax[0,0].plot(tt.hand[0,:],tt.hand[1,:])
    ax[0,0].set_xlim(-.4,.4)
    ax[0,0].set_ylim(.1,.9)

for i in range(0,distance.shape[0]):
  ax[1,0].plot(distance[i],peakspeed[i],linestyle = None,linewidth = 0, marker = ".")
  ax[1,0].set_xlim(0,1)
  ax[1,0].set_ylim(0,1)

for i in range(0,distance.shape[0]):
  ax[1,1].plot(distance[i],duration[i],linestyle = None,linewidth = 0, marker = ".")
ax[1,1].set_xlim(0,1)
ax[1,1].set_ylim(0,3)

# %%
saveDict = {
  "peakspeed":peakspeed,
  "duration":duration,
  "distance":distance,
  "timeValuation":timeValuation,
  "moveNumber":moveNumber,
  "trajs":trajList}
import scipy
scipy.io.savemat("SimulationsEveryday"+"Sweep.mat", saveDict)
# %%
