#%%
# go magic trick
import ReachingModels as reaching
import SimpleOpt as so
import numpy as np
import matplotlib.pyplot as plt
import colour as clr
import plotpresentlib as pp
from matplotlib.figure import Figure
import PreferredReachExperiment as prefdur
import os
import scipy.io as sio
import time as time

#%%

def single_grp_fit(ct,condition = 'eday', dataset_kind = 'AllMov', the_distance_thresh = 0.05, the_distance_des = 0.3, num_max_rolls = 5):
  # def magic_trick(condition = 'eday', dataset_kind = 'AllMov', the_distance_thresh = 0.05, the_distance_des = 0.3, num_max_rolls = 5):
  # the_distance_des = 0.3
  # the_distance_thresh = 0.05
  # condition = 'Eday'
  # dataset_kind   = 'AllMov' # or 'ByTgt'
  # num_max_rolls = 5

  msCTs = []
  out_r2_dur = []
  out_r2_spd = []
  out_perc_rms_dur = []
  out_perc_rms_spd = []
  out_sols = []
  for i in range(1,10):
    reachingPath = os.path.dirname(reaching.__file__)
    fmodel = 'modelSubj'+str(i) + '.mat'
    model = reaching.Kinarm(reachingPath + '/empirical/subdata/'+fmodel)
    fdata = 'MainSeq' + condition + 'subj' + str(i) + dataset_kind + '.mat'
    sdat = sio.loadmat(reachingPath + '/empirical/subdata/'+fdata) #load test data (change path)

    ms_hum = prefdur.readMSFromMatStruct(sdat,kind=dataset_kind)

    # draw an index randomly from the 'distance' variable, where distance is within 0.05 cm of the variable distance_draw
    ms_mod = prefdur.optimalReachesForCT(ms_hum,model,ct)
    out_r2_dur.append(prefdur.R2(ms_mod.duration,ms_hum.duration))
    out_r2_spd.append(prefdur.R2(ms_mod.peakspeed,ms_hum.peakspeed))
    out_perc_rms_dur.append(prefdur.perc_rms(ms_mod.duration,ms_hum.duration))
    out_perc_rms_spd.append(prefdur.perc_rms(ms_mod.peakspeed,ms_hum.peakspeed))
    out_sols.append(ms_mod)

  return out_perc_rms_dur, out_perc_rms_spd, out_r2_dur, out_r2_spd, msCTs, sols, ms_hum
    
#%%
def magic_trick(condition = 'eday', dataset_kind = 'AllMov', the_distance_thresh = 0.05, the_distance_des = 0.3, num_max_rolls = 5):
  # def magic_trick(condition = 'eday', dataset_kind = 'AllMov', the_distance_thresh = 0.05, the_distance_des = 0.3, num_max_rolls = 5):
  # the_distance_des = 0.3
  # the_distance_thresh = 0.05
  # condition = 'Eday'
  # dataset_kind   = 'AllMov' # or 'ByTgt'
  # num_max_rolls = 5

  msCTs = []
  out_r2_dur = []
  out_r2_spd = []
  out_perc_rms_dur = []
  out_perc_rms_spd = []
  out_sols = []
  for i in range(1,10):
    reachingPath = os.path.dirname(reaching.__file__)
    fmodel = 'modelSubj'+str(i) + '.mat'
    model = reaching.Kinarm(reachingPath + '/empirical/subdata/'+fmodel)
    fdata = 'MainSeq' + condition + 'subj' + str(i) + dataset_kind + '.mat'
    sdat = sio.loadmat(reachingPath + '/empirical/subdata/'+fdata) #load test data (change path)

    ms_hum = prefdur.readMSFromMatStruct(sdat,kind=dataset_kind)

    # draw an index randomly from the 'distance' variable, where distance is within 0.05 cm of the variable distance_draw
    idx_, = np.where(np.abs(ms_hum.distance-the_distance_des)<the_distance_thresh)

    r2_dur = []
    r2_spd = []
    perc_rms_dur = []
    perc_rms_spd = []
    sols = []
    max_range = num_max_rolls if idx_.shape[0] > num_max_rolls else idx_.shape[0]
    for j in range(0,max_range):
      idx = idx_[j]

      # extract the start and end points for the idx'th trial
      xystartj  = ms_hum.xystart[idx,:]
      xyendj    = ms_hum.xyend[idx,:]
      durationj = ms_hum.duration[idx]

      # now call movementTimeOptSetup to set up the optimization problem, and solve it.
      optVars = model.movementTimeOptSetup(theN = 20, theDuration = durationj)
      solP, optRes = model.updateGuessAndSolve(optVars, xystartj, xyendj, theGeneratePlots = 0)

      # extract the con_time variable from the solution
      ct = solP.con_time
      msCTs.append(solP.con_time)
      sols.append(solP)

      # call prefdur.optimalReachesForCT to get the optimal reaches for the given c_t
      ms_mod = prefdur.optimalReachesForCT(ms_hum,model,ct)
      r2_dur.append(prefdur.R2(ms_mod.duration,ms_hum.duration))
      r2_spd.append(prefdur.R2(ms_mod.peakspeed,ms_hum.peakspeed))
      perc_rms_dur.append(prefdur.perc_rms(ms_mod.duration,ms_hum.duration))
      perc_rms_spd.append(prefdur.perc_rms(ms_mod.peakspeed,ms_hum.peakspeed))
    
    out_r2_dur.append(r2_dur)
    out_r2_spd.append(r2_spd)
    out_perc_rms_dur.append(perc_rms_dur)
    out_perc_rms_spd.append(perc_rms_spd)
    out_sols.append(sols)

  return out_perc_rms_dur, out_perc_rms_spd, out_r2_dur, out_r2_spd, msCTs, sols, ms_hum
# %%
out_perc_rms_dur, out_perc_rms_spd, out_r2_dur, out_r2_spd, msCTs, sols, ms_hum = magic_trick(condition = 'eday', dataset_kind = 'AllMov', the_distance_thresh = 0.05, the_distance_des = 0.3, num_max_rolls = 5)

#%%
#visualize the results. using plt, plot all of the out_perc_rms_spd and out_perc_rms_dur
f,ax = plt.subplots(2,2)
#use layout to pad more on the sides by 2
f.tight_layout(h_pad=2,w_pad = 2)

[ax[0,0].plot(out_perc_rms_dur[i],marker='.',linestyle='none') for i in range(0,9)]
ax[0,0].set_title('perc rms dur')

[ax[0,1].plot(out_perc_rms_spd[i],marker='.',linestyle='none') for i in range(0,9)]
ax[0,1].set_title('perc rms speed')

[ax[1,1].plot(msCTs[i],marker='.',linestyle='none') for i in range(0,9)]
ax[1,1].set_title('C_T')


# %%
# list comprehension to get mean of output variables
mean_perc_rms_dur = [np.mean(out_perc_rms_dur[i]) for i in range(0,9)]
mean_perc_rms_spd = [np.mean(out_perc_rms_dur[i]) for i in range(0,9)]
mean_r2_dur = [np.mean(out_r2_dur[i]) for i in range(0,9)]
mean_r2_spd = [np.mean(out_r2_spd[i]) for i in range(0,9)]


# %%
