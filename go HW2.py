#%%
# Step 1: Harris and Wolpert grid sweep across C_T, C_FR
#Here I try playing with the Harris & Wolpert 1998 model. 

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import colour as clr
import sys
sys.path.insert(0,'parameterFiles')

import ReachingModels as reaching

sim = reaching.eyeHW2()

loop_dist = np.array([0.1,1,10,100])
loop_cfr = np.array([.01,.1,1,10])
#allocate for storage:durations, speeds, costs, rewards, solved --> 
M = len(loop_cfr)
N = len(loop_dist)
duration_vec  = np.zeros([M, N])
peakspeed_vec = np.zeros([M, N])
J             = np.zeros([M, N])
costFR        = np.zeros([M, N])
costWork      = np.zeros([M, N])
costTime      = np.zeros([M, N])
solved        = np.zeros([M, N])
distances     = np.zeros([M, N])
ct_vec        = np.zeros([M, N])
traj_bycfr    = list()

c_t   = 10
x     = 0.0

for i, c_fr in enumerate(loop_cfr):
  traj_inner = list()
  for j, dist in enumerate(loop_dist):
    optiPrev = sim.movementTimeOptSetup(
      theTimeValuation  = .1,
      theN              = 100,   #2022-10: 20 and 40 both work flawlessly. this is after removing accel = 0 constraint. 
      theFRCoef         = c_fr)

    traj, optiOrig = sim.updateGuessAndSolve(
      optiPrev, 
      np.array([x]), 
      np.array([x]) + np.array([dist]),
      theDurationGuess    = 1.0,
      theTimeValuation    = c_t,
      theGeneratePlots    = 0,
      theFRCoef = c_fr)
    duration_vec[i,j]  = traj.duration
    peakspeed_vec[i,j] = traj.vecspeed.max()
    J[i,j]             = traj.costJ
    costFR[i,j]        = traj.costFR
    costWork[i,j]      = traj.costWork
    costTime[i,j]      = traj.costTime
    solved[i,j]        = traj.solved
    distances[i,j]     = dist
    ct_vec[i,j]        = c_t
    traj_inner.append(traj)
  traj_bycfr.append(traj_inner)

#%%
# create color gradient tvalcolors.
color1 = clr.Color("#e0f3db")
tvalcolors = list(color1.range_to(clr.Color("#084081"),len(loop_cfr)))

fig,ax = plt.subplots(2,2)
fig.tight_layout(h_pad=2,w_pad = 2)

for i in range(len(traj_bycfr)):
  for j in range(len(traj_bycfr[i])):
    ax[0,0].plot(traj_bycfr[i][j].time,traj_bycfr[i][j].vecspeed)
fig.show()
#%%

ax[0,0].plot(traj.time,traj.hand[0,:])
ax[0,1].plot(traj.time,traj.vecspeed)
ax[1,1].plot(traj.time,traj.uraterate)

# %%
