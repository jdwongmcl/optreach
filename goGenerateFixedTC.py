#%% Loop across distances and valuations to generate double-pendulum distance/time predictions. 
import ReachingModels as reaching
import numpy as np
import matplotlib.pyplot as plt
import colour as clr
import plotpresentlib as pp
from matplotlib.figure import Figure

DOSAVE = 1
savename = 'fixedTC10'
%config InlineBackend.figure_formats = ['svg']
import sys
sys.path.insert(0,'parameterFiles')

# model that has equations to compute endpoint jacobians, equations of motion, and energy. 
sim = reaching.Kinarm()
sim.l1 = 0.35
sim.l = [sim.l1,sim.l2]
#sim = reaching.PointMass()

# starting x and y
x = 0.0
ys = 0.25
xystart = np.array([x,ys])

# loopValuation = [11] # for fit

# np1 = np.linspace(0.01,0.50,num=50)
# np2 = np.linspace(0.55,0.60,num=2)
# loopdist = np.concatenate((np1,np2))
loopdist = np.linspace(0.01,0.5,num=50)

loopDuration = np.linspace(0.1,2.5,num=25)

# create color gradient tvalcolors.
color1 = clr.Color("#e0f3db")
tvalcolors = list(color1.range_to(clr.Color("#084081"),len(loopDuration)))

### allocate for storage:durations, speeds, costs, rewards, solved
M = len(loopDuration)
N = len(loopdist)
duration      = np.zeros([M, N])
peakhandspeed = np.zeros([M, N])
J             = np.zeros([M, N])
costFR        = np.zeros([M, N])
costWork      = np.zeros([M, N])
costTime      = np.zeros([M, N])

solved        = np.zeros([M, N])
distance     = np.zeros([M, N])
timeValuation = np.zeros([M, N])

# set the fixed time cost.
tcFixed = 10

# initialize trajectories as a 1d list of 1d lists. 
traj = [[0 for x in range(N)] for y in range(M)] 

### setup the optimization. 
optiPrev = sim.movementTimeOptSetup(
  theN              = 20,   #2022-10: 20 and 40 both work flawlessly. this is after removing accel = 0 constraint. 
  theFRCoef         = 0.085, 
  theTimeValuation  = tcFixed,
  theDuration = 1)

### solve the opt once, we can then always use optiOrig as initial guess. 
trajOrig, optiOrig = sim.updateGuessAndSolve(
  optiPrev, 
  xystart, 
  xystart + np.array([x,.10]),
  theDurationGuess    = 1,
  theTimeValuation    = 10,
  theGeneratePlots    = 0)

dGuess = trajOrig.duration

for (nDist, valdist) in enumerate(loopdist):  
  for (mDur, valDur) in enumerate(loopDuration):
    optiPrev = sim.movementTimeOptSetup(
      theN              = 20,   #2022-10: 20 and 40 both work flawlessly. this is after removing accel = 0 constraint. 
      theFRCoef         = 0.085, 
      theTimeValuation  = tcFixed,
      theDuration = valDur)

    trajResult, optiReturn = sim.updateGuessAndSolve(
      optiPrev, 
      xystart, 
      xystart + np.array([0,valdist]), 
      theDurationGuess  = valDur,
      theTimeValuation    = tcFixed,
      theGeneratePlots    = 0)
    
    # if we found a solution:
    if trajResult.solved:
      # unpack the results
      duration[mDur,nDist]      = trajResult.duration
      J[mDur,nDist]             = trajResult.costJ
      costFR[mDur,nDist]        = trajResult.costFR
      costWork[mDur,nDist]      = trajResult.costWork
      costTime[mDur,nDist]      = trajResult.costTime
      peakhandspeed[mDur,nDist] = trajResult.peakspeed
      solved[mDur,nDist]        = trajResult.solved
      distance[mDur,nDist]     = valdist
      timeValuation[mDur,nDist] = tcFixed
      # update the optiPrev, only if solved == true.
      #optiPrev  = optiReturn
      dGuess    = trajResult.duration
      traj[mDur][nDist]=trajResult
    
    #increment inner

if DOSAVE:
  saveDict = {
    "duration":duration,
    "peakspeed":peakhandspeed,
    "distance":distance,
    "timeValuation":timeValuation,
    "traj":traj}
  import scipy
  scipy.io.savemat('simulationResults/'+savename+'.mat', saveDict)

# %%
